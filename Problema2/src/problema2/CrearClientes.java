/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problema2;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Hevia
 */
class CrearClientes implements Runnable
{
    Barberia tienda;
    int numero = 1;
 
    public CrearClientes(Barberia shop)
    {
        this.tienda = shop;
    }
 
    public void run()
    {
        while(true)
        {
            Cliente clientes = new Cliente(tienda);
            clientes.setTiempo(new Date());
            
            Thread hilocliente = new Thread(clientes);
            clientes.setNombre(" [Cliente " + numero + "]");
            numero ++;
            
            hilocliente.start();
 
            try
            {
                TimeUnit.SECONDS.sleep((long)(Math.random()*12));
            }
            catch(InterruptedException iex)
            {
                iex.printStackTrace();
            }
        }
    }
 
}