/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problema2;

import java.util.Date;

/**
 *
 * @author Hevia
 */
class Cliente implements Runnable
{
    String nombre;
    Date tiempo;
    Barberia getTiempo;
 
    public Cliente(Barberia tienda)
    {
        this.getTiempo = tienda;
    }

    public String getName() {
        return nombre;
    }
 
    public Date getInTime() {
        return tiempo;
    }
 
    public void setNombre(String name) {
        this.nombre = name;
    }
 
    public void setTiempo(Date inTime) {
        this.tiempo = inTime;
    }
 
    public void run()
    {
        cortarPelo();
    }
    private synchronized void cortarPelo()
    {
        getTiempo.añadircliente(this);
    }
}