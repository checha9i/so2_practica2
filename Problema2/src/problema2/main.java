/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problema2;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
/**
 *
 * @author Javier Hevia
 */
public class main {
 
    public static void main(String a[])
    {
        Barberia tienda = new Barberia();
 
        Barbero barbero = new Barbero(tienda);
    
        CrearClientes cliente = new CrearClientes(tienda);
 
        // hilos parar la implementación
        Thread tiendabarbero = new Thread(barbero);
        Thread hcliente = new Thread(cliente);
        
        // hilos run
        hcliente.start();
        tiendabarbero.start();
    }
}
