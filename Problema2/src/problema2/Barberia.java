/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problema2;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Hevia
 */
class Barberia {

    int NumeroDeSillas;
    List<Cliente> ListaCliente;
    Boolean dormir = false;
    
    public Barberia() {
        NumeroDeSillas = 20;
        ListaCliente = new LinkedList<Cliente>();
    }

    public void cortarpelo() {
        Cliente cliente;
        System.out.println("Barbero caminando a la sala de espera");
        synchronized (ListaCliente) {

            while (ListaCliente.size() == 0) {
                System.err.println("No hay nadie en la sala...");
                System.err.println("Barbero se pone a dormir....");
                dormir = true;
                try {
                    ListaCliente.wait(); // se queda esperando.
                } catch (InterruptedException iex) {
                    iex.printStackTrace();
                }
            }
            System.out.println("Barbero llama a un cliente de la sala de espera.....");
            cliente = (Cliente) ((LinkedList<?>) ListaCliente).poll(); // saco al primer cliente para atenderlo
        }
        long tiempoduracion = 0;
        try {
            System.out.println("Corta el cabello del cliente : " + cliente.getName());
            System.out.println("*************************************************************************************");
            tiempoduracion = (long) (Math.random() * 10);
            TimeUnit.SECONDS.sleep(tiempoduracion);
        } catch (InterruptedException iex) {
            iex.printStackTrace();
        }
        System.out.println("*************************************************************************************");
        System.out.println("Corte de cabello completo del cliente : " + cliente.getName() + " en " + tiempoduracion + " segundos.");
    }

    public void añadircliente(Cliente cliente) {
        if (dormir) {
            System.out.println("        Barbero se despierta, ya hay clientes en la sala");
            dormir = false;
        }
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Cliente.... : " + cliente.getName() + " entrando a la baberia en " + cliente.getInTime().getHours() +":"+ cliente.getInTime().getMinutes());

        synchronized (ListaCliente) {
            if (ListaCliente.size() == NumeroDeSillas) {
                System.err.println("No hay silla disponible para el cliente " + cliente.getName());
                System.err.println("Cliente.. " + cliente.getName() + " ... se va");
                return;
            }

            ((LinkedList<Cliente>) ListaCliente).offer(cliente); // agrega un elemento de ultimo en la lista
            System.out.println("Cliente.... : " + cliente.getName() + " consiguió la silla.");

            if (ListaCliente.size() == 1) {
                ListaCliente.notify(); // Arranca el hilo de la lista
            }
        }
        System.out.println("-------------------------------------------------------------------------------------");
    }
}
