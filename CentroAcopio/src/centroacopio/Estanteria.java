/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package centroacopio;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author quiquemazariegos
 */
public class Estanteria<E> {

    private int max_space;
    private LinkedList<E> shelving_queue;

    private ReentrantLock block = new ReentrantLock(true);
    private Condition no_full = block.newCondition();
    private Condition no_empty = block.newCondition();

    public Estanteria() {
        this.max_space = 20;
        this.shelving_queue = new LinkedList<>();
    }

    public void PlaceBox(E element) {
        block.lock();
        try {
            while (shelving_queue.size() == max_space) {
                no_full.wait();
            }

            shelving_queue.add(element);
            no_empty.signalAll();
        } catch (Exception e) {
            System.out.println("cca " + e);
        } finally {
            block.unlock();
        }

    }

    public E RemoveBox() {
        block.lock();
        try {
            while (shelving_queue.size() == 0) {
                no_empty.wait();
                System.out.println("esperando a que exista caja");
            }

            E element = shelving_queue.remove();
            no_full.signalAll();
            return element;
        } catch (Exception e) {
            System.out.println("caca");
            return null;
        } finally {
            block.unlock();
        }
    }

    public void printActions() {
        for (int i = 0; i < this.shelving_queue.size(); i++) {
            System.out.println("Se inserto en: [" + i + "] - valor: " + shelving_queue.get(i));
        }
    }
}
