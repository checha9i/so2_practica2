/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package centroacopio;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author quiquemazariegos
 */
public class PeopleThread implements Runnable {

    Estanteria<Integer> shelving_queue;
    int index;

    public PeopleThread(Estanteria<Integer> shelving_queue, int i) {
        this.shelving_queue = shelving_queue;
        this.index = i;
    }

    @Override
    public void run() {
       Random number_of_people = new Random();
        int r_number = number_of_people.nextInt(2);
        if (r_number == 0) {
            sleep();
            synchronized(shelving_queue){
                shelving_queue.PlaceBox(index);
            }
            

        } else {
            sleep();
            synchronized(shelving_queue){
                shelving_queue.RemoveBox();
            }
            
        }

    }

    private static void sleep() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
