/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package centroacopio;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
/**
 *
 * @author quiquemazariegos
 */
public class CentroAcopio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ExecutorService  ejecutor = Executors.newCachedThreadPool();
        
        Estanteria<Integer> queue = new Estanteria();
        
        for (int i = 0; i < 100; i++) {
            ejecutor.execute( new PeopleThread(queue,i));
            
        }
        
        
        ejecutor.shutdown();
        
        try {
            ejecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            queue.printActions();
        } catch (Exception e) {
        }
    }
    
}
