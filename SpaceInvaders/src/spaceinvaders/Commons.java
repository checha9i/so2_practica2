package spaceinvaders;

public interface Commons {

    int NUMBER_OF_ALIENS_TO_DESTROY = 20;
    int BOARD_WIDTH = 400;
    int BOARD_HEIGHT = 400;
    int CHANCE = 5;
    int DELAY = 17;
    int PLAYER_WIDTH = 15;
    int PLAYER_HEIGHT = 10;
    int BORDER_RIGHT = 30;
    int BOMB_HEIGHT = 5;
    int ALIEN_HEIGHT = 12;
    int ALIEN_WIDTH = 12;
    int ALIEN_INIT_X = 150;
    int ALIEN_INIT_Y = 5;
    int BORDER_LEFT = 5;
    int GROUND = 290;
    int GO_DOWN = 15;

}
