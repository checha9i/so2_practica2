package spaceinvaders;

import objects.Enemigo;
import objects.Jugador;
import objects.Disparo;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.Timer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.MediaTracker;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import objects.Jugador2;

public class Juego extends JPanel {

    private Dimension d;
    private List<Enemigo> aliens;
    public static Jugador player;
    public static Jugador2 player2;
    private Disparo shot;
    private Disparo shot2;
    
    private int direction = -1;
    private int deaths = 0;

    private boolean inGame = true;
    private String explImg = "src/img/explosion.png";
    

    private Timer timer;

    public Juego() {
        initBoard();
        gameInit();
        setLayout(null);
    }

    private void initBoard() {
        addKeyListener(new TAdapter());
        addKeyListener(new TAdapter2());
        setFocusable(true);
        d = new Dimension(Commons.BOARD_WIDTH, Commons.BOARD_HEIGHT);
        setBackground(Color.black);
        timer = new Timer(Commons.DELAY, new GameCycle());
        timer.start();
        gameInit();
    }


    private void gameInit() {

        aliens = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                Enemigo alien = new Enemigo(Commons.ALIEN_INIT_X + 18 * j,
                Commons.ALIEN_INIT_Y + 18 * i);
                aliens.add(alien);
            }
        }

        player = new Jugador();
        player2 = new Jugador2();
        shot = new Disparo();
        shot2 = new Disparo();
    }

    private void dibujarEnemigos(Graphics g) {

        for (Enemigo alien : aliens) {

            if (alien.isVisible()) {

                g.drawImage(alien.getImage(), alien.getX(), alien.getY(), this);
            }

            if (alien.isDying()) {

                alien.die();
            }
        }
    }

    private void dibujarJugador(Graphics g) {

        if (player.isVisible()) {

            g.drawImage(player.getImage(), player.getX(), player.getY(), this);
        }

        
        if (player.isDying()&&player2.isDying()) {

            player.die();
            player2.die();
            inGame = false;
        }else if(player.isDying()&&!player2.isDying()){
            player.die();
            inGame = true;
        }
        
        
        
    }

private void dibujarJugador2(Graphics g) {

        if (player2.isVisible()) {

            g.drawImage(player2.getImage(), player2.getX(), player2.getY(), this);
        }

      if (player.isDying()&&player2.isDying()) {

            player.die();
            player2.die();
            inGame = false;
        }else if(!player.isDying()&&player2.isDying()){
            player2.die();
            inGame = true;
        }
        
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        dibujar(g);
    }

    private void dibujar(Graphics g) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image image = toolkit.getImage("src/img/bg.jpg");
        g.setColor(Color.black);
        g.fillRect(0, 0, d.width, d.height);
        //g.drawImage(image, 0, 0, null);
        g.setColor(Color.green);

        if (inGame) {
            g.drawLine(0, Commons.GROUND,
            Commons.BOARD_WIDTH, Commons.GROUND);
            dibujarEnemigos(g);
            dibujarJugador(g);
            dibujarJugador2(g);
            dibujarDisparo(g);
            dibujarDisparo2(g);
            dibujarBomba(g);
        } else {

            if (timer.isRunning()) {
                timer.stop();
            }

            //GAME OVER
            if(!Tablero.mensaje.equalsIgnoreCase("Game won!")){
            Tablero.mensaje = "Game Over!";
            }else{
             Tablero.mensaje = "Game won!";
            }
            
        }

        Toolkit.getDefaultToolkit().sync();
    }

   
    private void dibujarDisparo(Graphics g) {

        if (shot.isVisible()) {

            g.drawImage(shot.getImage(), shot.getX(), shot.getY(), this);
        }
    }
    private void dibujarDisparo2(Graphics g) {

        if (shot2.isVisible()) {

            g.drawImage(shot2.getImage(), shot2.getX(), shot2.getY(), this);
        }
    }
    
    private void dibujarBomba(Graphics g) {

        for (Enemigo a : aliens) {

            Enemigo.Bomba b = a.getBomba();

            if (!b.isDestroyed()) {
                g.drawImage(b.getImage(), b.getX(), b.getY(), this);
            }
        }
    }

    private void update() {

        if (deaths == Commons.NUMBER_OF_ALIENS_TO_DESTROY) {

            inGame = false;
            timer.stop();
            Tablero.mensaje = "Game won!";
            
        }

        // player
        player.disparar();


        // shot
        if (shot.isVisible()) {

            int shotX = shot.getX();
            int shotY = shot.getY();

            for (Enemigo alien : aliens) {

                int alienX = alien.getX();
                int alienY = alien.getY();

                if (alien.isVisible() && shot.isVisible()) {
                    if (shotX >= (alienX)
                            && shotX <= (alienX + Commons.ALIEN_WIDTH)
                            && shotY >= (alienY)
                            && shotY <= (alienY + Commons.ALIEN_HEIGHT)) {
                        shot.die();
                    alien.restarVida();
                        
                        if(alien.getVidas()==0){
                        ImageIcon ii = new ImageIcon(explImg);
                        alien.setImage(ii.getImage());
                        alien.setDying(true);
                        deaths++;

                        }
                    }
                }
            }

            int y = shot.getY();
            y -= 4;

            if (y < 0) {
                shot.die();
            } else {
                shot.setY(y);
            }
        }
        player2.disparar();
         // shot2
        if (shot2.isVisible()) {

            int shot2X = shot2.getX();
            int shot2Y = shot2.getY();
            for (Enemigo alien : aliens) {

                int alienX = alien.getX();
                int alienY = alien.getY();

                if (alien.isVisible() && shot2.isVisible()) {
                    if (shot2X >= (alienX)
                            && shot2X <= (alienX + Commons.ALIEN_WIDTH)
                            && shot2Y >= (alienY)
                            && shot2Y <= (alienY + Commons.ALIEN_HEIGHT)) {
                        shot2.die();
                        alien.restarVida();
                        if(alien.getVidas()==0){
                        ImageIcon ii = new ImageIcon(explImg);
                        alien.setImage(ii.getImage());
                        alien.setDying(true);
                        deaths++;
                        }
                    }
                }
            }

            int y = shot2.getY();
            y -= 4;

            if (y < 0) {
                shot2.die();
            } else {
                shot2.setY(y);
            }
        }
        
        // aliens

        for (Enemigo alien : aliens) {

            int x = alien.getX();

            if (x >= Commons.BOARD_WIDTH - Commons.BORDER_RIGHT && direction != -1) {

                direction = -1;

                Iterator<Enemigo> i1 = aliens.iterator();

                while (i1.hasNext()) {

                    Enemigo a2 = i1.next();
                    a2.setY(a2.getY() + Commons.GO_DOWN);
                }
            }

            if (x <= Commons.BORDER_LEFT && direction != 1) {

                direction = 1;

                Iterator<Enemigo> i2 = aliens.iterator();

                while (i2.hasNext()) {

                    Enemigo a = i2.next();
                    a.setY(a.getY() + Commons.GO_DOWN);
                }
            }
        }

        Iterator<Enemigo> it = aliens.iterator();

        while (it.hasNext()) {

            Enemigo alien = it.next();

            if (alien.isVisible()) {

                int y = alien.getY();

                if (y > Commons.GROUND - Commons.ALIEN_HEIGHT) {
                    inGame = false;
                    Tablero.mensaje = "Invasion!";
                }

                alien.disparar(direction);
            }
        }

        // bombs
        Random generator = new Random();

        for (Enemigo alien : aliens) {

            int shot = generator.nextInt(15);
            Enemigo.Bomba bomb = alien.getBomba();

            if (shot == Commons.CHANCE && alien.isVisible() && bomb.isDestroyed()) {
                bomb.setDestroyed(false);
                bomb.setX(alien.getX());
                bomb.setY(alien.getY());
            }
            
            int bombX = bomb.getX();
            int bombY = bomb.getY();
            int playerX = player.getX();
            int playerY = player.getY();

            if (player.isVisible() && !bomb.isDestroyed()) {

                if (bombX >= (playerX)
                        && bombX <= (playerX + Commons.PLAYER_WIDTH)
                        && bombY >= (playerY)
                        && bombY <= (playerY + Commons.PLAYER_HEIGHT)) {

                    bomb.setDestroyed(true);
                    player.restarVida();
                    
                    System.out.println("Vidas del jugador 1:"+player.getVidas());
                    if(player.getVidas()==0){
                    ImageIcon ii = new ImageIcon(explImg);
                    player.setImage(ii.getImage());
                    player.setDying(true);
                    
                    }
                }
            }
            
            int player2X = player2.getX();
            int player2Y = player2.getY();

            if (player2.isVisible() && !bomb.isDestroyed()) {

                if (bombX >= (player2X)
                        && bombX <= (player2X + Commons.PLAYER_WIDTH)
                        && bombY >= (player2Y)
                        && bombY <= (player2Y + Commons.PLAYER_HEIGHT)) {
                    bomb.setDestroyed(true);
                    player2.restarVida();
                    System.out.println("Vidas del jugador 2:"+player2.getVidas());
                    if(player2.getVidas()==0){
                    ImageIcon ii = new ImageIcon(explImg);
                    player2.setImage(ii.getImage());
                    player2.setDying(true);

                    }
                }
            }

            if (!bomb.isDestroyed()) {

                bomb.setY(bomb.getY() + 1);

                if (bomb.getY() >= Commons.GROUND - Commons.BOMB_HEIGHT) {

                    bomb.setDestroyed(true);
                }
            }
        }
    }

    private void CicloJuego() {
        update();
        repaint();
    }

    private class GameCycle implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            CicloJuego();
        }
    }

    private class TAdapter extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {

            player.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {

            player.keyPressed(e);

            int x = player.getX();
            int y = player.getY();

            int key = e.getKeyCode();

            if (key == KeyEvent.VK_S&&!Juego.player.isDie()) {

                if (inGame) {

                    if (!shot.isVisible()) {

                        shot = new Disparo(x, y);
                    }
                }
            }
        }
    }
    
    
    private class TAdapter2 extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {

            player2.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {

            player2.keyPressed(e);

            int x = player2.getX();
            int y = player2.getY();

            int key = e.getKeyCode();

            if (key == KeyEvent.VK_K&&!Juego.player2.isDie()) {

                if (inGame) {

                    if (!shot2.isVisible()) {

                        shot2 = new Disparo(x, y);
                    }
                }
            }
        }
    }
    
}
