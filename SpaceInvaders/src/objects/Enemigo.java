package objects;

import javax.swing.ImageIcon;

public class Enemigo extends Sprite {

    private Bomba bomba;
    public int vidas=3;
    public Enemigo(int x, int y) {
        
        initAlien(x, y);
    }

    private void initAlien(int x, int y) {
        /*
         * Setear la posicion del enemigo
         */
        this.x = x;
        this.y = y;

        /*
         * Nueva bombaa
         */
        bomba = new Bomba(x, y);

        String img = "src/img/alien.png";
        ImageIcon i = new ImageIcon(img);

        setImage(i.getImage());
    }

    public void disparar(int direccion) {
        this.x += direccion;
    }

    public Bomba getBomba() {
        /*
         * get Bomba
         */
        return bomba;
    }

     public int getVidas(){
        return vidas;
    }

      public void restarVida(){
        vidas-=1;
    }
    
    public class Bomba extends Sprite {

        private boolean destroyed;

        public Bomba(int x, int y) {

            initBomba(x, y);
        }

        private void initBomba(int x, int y) {

            setDestroyed(true);
            
            /*
             * Setear la posicion de la bomba
             */
            this.x = x;
            this.y = y;

            String img = "src/img/bomb.png";
            ImageIcon i = new ImageIcon(img);
            setImage(i.getImage());
        }

       
    
        
        public void setDestroyed(boolean destroyed) {
            this.destroyed = destroyed;
        }

        public boolean isDestroyed() {
            return destroyed;
        }
    }
}
