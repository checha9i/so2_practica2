package objects;
import spaceinvaders.Commons;
import javax.swing.ImageIcon;
import java.awt.event.KeyEvent;
import spaceinvaders.Juego;

public class Jugador extends Sprite {

    private int anchura;
    public int vidas=3;
    
    
    public Jugador() {

        initPlayer();
    }

    private void initPlayer() {

        String img = "src/img/player.png";
        ImageIcon i = new ImageIcon(img);

        anchura = i.getImage().getWidth(null);
        setImage(i.getImage());

        /*
         * Posicion Inicial del Jugador
         */
        
        int init_X = 220;
        setX(init_X);
    
    
        int init_Y = 270;
        setY(init_Y);
    }
    
    public int getVidas(){
        return vidas;
    }

    public void restarVida(){
        vidas-=1;
    }
    
    public void disparar() {
        
        /*
         * Al moverse se actualiza la nueva posicion
         */
    
        if(!Juego.player.isDie()&&!Juego.player2.isDie()){
        if((Juego.player.getX()+dx<Juego.player2.getX()-15)){
        x += dx;
        if (x <= 2) {
            x = 2;
        }
        }
       if(Juego.player.getX()>(Juego.player2.getX()+dx)-15){
                    x=Juego.player2.getX()-15-dx;
                }
        
        }else{
            x += dx;
        if (x <= 2) {
            x = 2;
        }
        }
        
       
        if (x >= Commons.BOARD_WIDTH - 2 * anchura) {
            x = Commons.BOARD_WIDTH - 2 * anchura;
        }
        
        
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        
        /*
         * Agregamos las acciones de las teclas
         */
        
        if (key == KeyEvent.VK_A) {
            dx = -2;
        }

        
        if (key == KeyEvent.VK_D) {
        
            dx = 2;
        
        }    
        
        
        
        
    }

    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        /*
         * Limpiamos la accion de la tecla
         */
        
        if (key == KeyEvent.VK_A) {
            dx = 0;
        }

        if (key == KeyEvent.VK_D) {
            dx = 0;
        }
    }
}
