package objects;

import javax.swing.ImageIcon;

public class Disparo extends Sprite {

    public Disparo() {
    }

    public Disparo(int x, int y) {
        initShot(x, y);
    }

    /*
     * Clase que controla el funcionamiento del disparo
     */
    private void initShot(int x, int y) {

        String img = "src/img/shot.png";
        ImageIcon i = new ImageIcon(img);
        setImage(i.getImage());
        
        int POS_X = 6;
        setX(x + POS_X);
        
        int POS_Y = 1;
        setY(y - POS_Y);
    }
}
