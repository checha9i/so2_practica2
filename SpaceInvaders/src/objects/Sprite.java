package objects;

import java.awt.Image;
import javax.swing.JLabel;

public class Sprite extends JLabel{

    
    /*
     * Clase que sirve como base para 
     * los demas objetos.
     */
    
    private boolean visible;
    private Image image;
    private boolean dying;
    private boolean die;

    /*
     * Posiciones
     * 
     */
    int x;
    int y;
   public int dx;

    public Sprite() {
        /*
         * Se inicializa como visible.
         */
        visible = true;
        die=false;
    }

    public boolean isVisible() {

        return visible;
    }
    
    public void die() {
        /*
         * Si muere ya no es visible
         */
        visible = false;
        die=true;
    }

    public boolean isDie(){
        return die;
    }
    
     public void setDying(boolean dying) {
        /*
         * Set Muerte
         */
        this.dying = dying;
    }

    public boolean isDying() {
        /*
         * Muere?
         */
        return this.dying;
    }

    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    
    public void setImage(Image image) {
        /*
         * Setea la imagen
         */
        this.image = image;
    }

    public Image getImage() {
        /*
         * Get imagen
         */
        return image;
    }

    public int getX() {
        /*
         * Get Posicion X
         */
        return x;
    }
    
    public void setX(int x) {
        /*
         * Set Posicion X
         */
        this.x = x;
    }

    public int getY() {
        /*
         * Get Posicion Y
         */
        return y;
    }

    
    public void setY(int y) {
        /*
         * Set Posicion Y
         */
        this.y = y;
    }

   
}
